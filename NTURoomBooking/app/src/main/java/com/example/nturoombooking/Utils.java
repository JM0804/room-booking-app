package com.example.nturoombooking;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;

// Reference: https://bezkoder.com/java-android-read-json-file-assets-gson/

public class Utils {
    public static String baseApiUrl = "https://jm0804-mpa.herokuapp.com/api";
    private static String iso8601 = "yyyy-MM-dd";
    @SuppressLint("SimpleDateFormat") public static SimpleDateFormat simpleDate =  new SimpleDateFormat(iso8601);

    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }
}

