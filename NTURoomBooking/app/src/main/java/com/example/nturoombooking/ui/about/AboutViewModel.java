package com.example.nturoombooking.ui.about;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AboutViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AboutViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue(
                "Designed and developed by Jonathan Mason for his year 3 Mobile Platform Applications coursework.\n\n" +
                        "Material Icons (c) Google used under Apache Licence 2.0.\n\n" +
                        "Maps (c) Mapbox used under BSD Licence.");
    }

    public LiveData<String> getText() {
        return mText;
    }
}