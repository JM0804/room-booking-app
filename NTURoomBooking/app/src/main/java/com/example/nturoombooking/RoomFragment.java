package com.example.nturoombooking;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import java.util.Objects;

public class RoomFragment extends DialogFragment {
    public static final String TAG = "RoomFragment";

    private MapView mapView;
    private Room room;

    // Reference: https://stackoverflow.com/a/15459259
    public static RoomFragment newInstance() {
        RoomFragment roomFragment = new RoomFragment();

        return roomFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
    }

    public void setObject(Room room) {
        this.room = room;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_room, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_mdi_arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ((ImageView) view.findViewById(R.id.capacity_icon))
                .setImageDrawable(createIcon(MaterialDrawableBuilder.IconValue.ACCOUNT_GROUP));
        ((ImageView) view.findViewById(R.id.computers_icon))
                .setImageDrawable(createIcon(MaterialDrawableBuilder.IconValue.DESKTOP_CLASSIC));
        ((ImageView) view.findViewById(R.id.has_projector_icon))
                .setImageDrawable(createIcon(MaterialDrawableBuilder.IconValue.PROJECTOR));
        ((ImageView) view.findViewById(R.id.type_icon))
                .setImageDrawable(createIcon(MaterialDrawableBuilder.IconValue.INFORMATION_OUTLINE));
        ((ImageView) view.findViewById(R.id.building_icon))
                .setImageDrawable(createIcon(MaterialDrawableBuilder.IconValue.CITY));

        assert getArguments() != null;

        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull final MapboxMap mapboxMap) {
                mapboxMap.setCameraPosition(new CameraPosition.Builder()
                    .target(getLatLng())
                        .zoom(19)
                        .build()
                );

                mapboxMap.getUiSettings().setAllGesturesEnabled(false);

                mapboxMap.setStyle(Style.DARK, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments.
                        String color = "#" + Integer.toHexString(getResources().getColor(R.color.colorPrimary)).substring(2);

                        CircleManager circleManager = new CircleManager(mapView, mapboxMap, style);
                        circleManager.create(new CircleOptions()
                            .withLatLng(getLatLng())
                                .withCircleColor(color)
                                .withCircleRadius(8f)
                        );
                    }
                });
            }
        });

        toolbar.setTitle(room.getName());

        ((TextView) view.findViewById(R.id.capacity)).setText(String.valueOf(room.getCapacity()));
        ((TextView) view.findViewById(R.id.computers)).setText(String.valueOf(room.getComputers()));
        ((TextView) view.findViewById(R.id.has_projector)).setText(hasProjector(room.getHasProjector()));
        ((TextView) view.findViewById(R.id.type)).setText(room.getType());
        ((TextView) view.findViewById(R.id.building)).setText(room.getBuildingLong());

        Button bookButton = view.findViewById(R.id.book_button);
        bookButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookRoomDialog bookRoomDialog = BookRoomDialog.newInstance(room.getName());

                FragmentTransaction ft = getParentFragmentManager().beginTransaction();
                bookRoomDialog.show(ft, RoomFragment.TAG);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    private LatLng getLatLng() {
        return new LatLng(room.getLat(), room.getLon());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    private String hasProjector(boolean hasProjector) {
        if (hasProjector) {
            return "Yes";
        }
        else {
            return "No";
        }
    }

    private Drawable createIcon(MaterialDrawableBuilder.IconValue iconValue) {
        return MaterialDrawableBuilder.with(getContext())
                .setIcon(iconValue)
                .setColor(Color.LTGRAY)
                .setSizeDp(35)
                .build();
    }
}
