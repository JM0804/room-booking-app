package com.example.nturoombooking;

public class Room {
    private String name;
    private String building;
    private String building_long;
    private String type;
    private int capacity;
    private int computers;
    private boolean has_projector;
    private Double lat;
    private Double lon;

    public Room() {

    }

    public Room(String name, String building, String building_long, String type, int capacity, int computers, boolean has_projector, Double lat, Double lon) {
        this.name = name;
        this.building = building;
        this.building_long = building_long;
        this.type = type;
        this.capacity = capacity;
        this.computers = computers;
        this.has_projector = has_projector;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public String getBuilding() {
        return building;
    }

    public String getBuildingLong() {
        return building_long;
    }

    public String getType() {
        return type;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getComputers() {
        return computers;
    }

    public boolean getHasProjector() {
        return has_projector;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }
}
