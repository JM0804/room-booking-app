package com.example.nturoombooking;

public class Building {
    private String name_short;
    private String name_long;

    public Building() {

    }

    public Building(String name_short, String name_long) {
        this.name_short = name_short;
        this.name_long = name_long;
    }

    public String getNameShort() {
        return name_short;
    }

    public String getNameLong() {
        return name_long;
    }
}
