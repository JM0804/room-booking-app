package com.example.nturoombooking.ui.rooms;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.nturoombooking.R;
import com.example.nturoombooking.Room;
import com.example.nturoombooking.RoomFilterDialog;
import com.example.nturoombooking.RoomFragment;
import com.example.nturoombooking.RoomListAdapter;
import com.example.nturoombooking.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.List;
import java.util.Objects;

public class RoomsFragment extends Fragment {

    private RoomsViewModel roomsViewModel;

    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        roomsViewModel =
                ViewModelProviders.of(this).get(RoomsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_rooms, container, false);
        /*final TextView textView = root.findViewById(R.id.text_rooms);
        roomsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        final ListView roomsListView = root.findViewById(R.id.rooms_list_view);

        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
            Request.Method.GET,
            (Utils.baseApiUrl + "/rooms"),
            null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    List<Room> rooms = new Gson().fromJson(
                            response.toString(),
                            new TypeToken<List<Room>>() {}.getType()
                    );
                    final RoomListAdapter roomsListAdapter = new RoomListAdapter(getContext(), rooms);
                    roomsListView.setAdapter(roomsListAdapter);
                    roomsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                            RoomFragment roomFragment = RoomFragment.newInstance();
                            roomFragment.setObject(roomsListAdapter.getItem(pos));

                            FragmentTransaction ft = getParentFragmentManager().beginTransaction();
                            roomFragment.show(ft, RoomFragment.TAG);

                        }
                    });
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
        );
        requestQueue.add(jsonArrayRequest);

        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RoomFilterDialog dialog = new RoomFilterDialog();
                FragmentTransaction ft = getParentFragmentManager().beginTransaction();
                dialog.show(ft, RoomFilterDialog.TAG);
            }
        });
        return root;
    }
}