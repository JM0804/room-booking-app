package com.example.nturoombooking;

import java.util.Date;

public class Booking {
    private String room;
    private String date;
    private int time;
    private int id;

    public Booking() {

    }

    public Booking(String room, String date, int time, int id) {
        this.room = room;
        this.date = date;
        this.time = time;
        this.id = id;
    }

    public String getRoom() {
        return room;
    }

    public String getDate() {
        return Utils.simpleDate.format(new Date(date));
    }

    public int getTime() {
        return time;
    }

    public int getId() {
        return id;
    }
}
