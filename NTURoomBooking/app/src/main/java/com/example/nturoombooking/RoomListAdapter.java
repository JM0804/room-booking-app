package com.example.nturoombooking;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import java.util.List;

public class RoomListAdapter extends BaseAdapter {
    private Context context;
    private final List<Room> rooms;

    public RoomListAdapter(Context context, List<Room> rooms)  {
        this.context = context;
        this.rooms = rooms;
    }

    @Override
    public int getCount() {
        return rooms.size();
    }

    @Override
    public Room getItem(int id) {
        return rooms.get(id);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @NonNull
    @Override
    public View getView(int pos, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        Drawable drawableCapacityIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.ACCOUNT_GROUP)
                .setColor(Color.LTGRAY)
                .build();

        Drawable drawableComputersIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.DESKTOP_CLASSIC)
                .setColor(Color.LTGRAY)
                .build();

        Drawable drawableHasProjectorIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.PROJECTOR)
                .setColor(Color.LTGRAY)
                .build();

        Drawable drawableTypeIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.INFORMATION_OUTLINE)
                .setColor(Color.LTGRAY)
                .build();

        Drawable drawableBuildingIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.CITY)
                .setColor(Color.LTGRAY)
                .build();

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.single_rooms_list_item, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.room_name);
            viewHolder.txtCapacity = convertView.findViewById(R.id.room_capacity);
            viewHolder.txtComputers = convertView.findViewById(R.id.room_computers);
            viewHolder.txtHasProjector = convertView.findViewById(R.id.room_has_projector);
            viewHolder.txtType = convertView.findViewById(R.id.room_type);
            viewHolder.txtBuilding = convertView.findViewById(R.id.room_building);

            viewHolder.imgCapacity = convertView.findViewById(R.id.room_capacity_icon);
            viewHolder.imgComputers = convertView.findViewById(R.id.room_computers_icon);
            viewHolder.imgHasProjector = convertView.findViewById(R.id.room_has_projector_icon);
            viewHolder.imgType = convertView.findViewById(R.id.room_type_icon);
            viewHolder.imgBuilding = convertView.findViewById(R.id.room_building_icon);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtName.setText(getItem(pos).getName());
        viewHolder.txtCapacity.setText(String.valueOf(getItem(pos).getCapacity()));
        viewHolder.txtComputers.setText(String.valueOf(getItem(pos).getComputers()));
        viewHolder.txtHasProjector.setText(hasProjector(getItem(pos).getHasProjector()));
        viewHolder.txtType.setText(getItem(pos).getType());
        viewHolder.txtBuilding.setText(getItem(pos).getBuilding());

        viewHolder.imgCapacity.setImageDrawable(drawableCapacityIcon);
        viewHolder.imgComputers.setImageDrawable(drawableComputersIcon);
        viewHolder.imgHasProjector.setImageDrawable(drawableHasProjectorIcon);
        viewHolder.imgType.setImageDrawable(drawableTypeIcon);
        viewHolder.imgBuilding.setImageDrawable(drawableBuildingIcon);
        return convertView;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtCapacity;
        TextView txtComputers;
        TextView txtHasProjector;
        TextView txtType;
        TextView txtBuilding;

        ImageView imgCapacity;
        ImageView imgComputers;
        ImageView imgHasProjector;
        ImageView imgType;
        ImageView imgBuilding;
    }

    private String hasProjector(boolean hasProjector) {
        if (hasProjector) {
            return "Yes";
        }
        else {
            return "No";
        }
    }
}