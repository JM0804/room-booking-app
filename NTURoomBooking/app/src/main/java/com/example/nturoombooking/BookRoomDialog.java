package com.example.nturoombooking;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BookRoomDialog extends DialogFragment {
    public static final String TAG = "BookRoom";

    // Reference: https://stackoverflow.com/a/15459259
    public static BookRoomDialog newInstance(String name) {
        BookRoomDialog bookRoomDialog = new BookRoomDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        bookRoomDialog.setArguments(args);

        return bookRoomDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.book_room_dialog, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_mdi_arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        assert getArguments() != null;

        toolbar.setTitle("Book " + getName());

        final Date[] chosenDate = {new Date()};
        final int[] chosenTime = {getHour()};

        final EditText dateSelector = view.findViewById(R.id.date);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()));
        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());

        dateSelector.setText(Utils.simpleDate.format(new Date()));
        dateSelector.setOnClickListener(new EditText.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Date newDate = new Date(year-1900, month, dayOfMonth);
            dateSelector.setText(Utils.simpleDate.format(newDate));
            datePickerDialog.updateDate(year, month, dayOfMonth);
            chosenDate[0] = newDate;
            }
        });

        final EditText startSelector = view.findViewById(R.id.start);
        startSelector.setText(String.format("%d:00", getHour()));

        final TimePickerDialog.OnTimeSetListener startPickerDialogCallback = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (minute > 0) {
                new AlertDialog.Builder(getContext())
                    .setTitle("Warning")
                    .setMessage("Time can only be set on the hour.\nSetting to " + String.valueOf(hourOfDay) + ":00.")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Ok
                        }
                    })
                    .create()
                    .show();
            }
            startSelector.setText(String.format("%d:00", hourOfDay));
            chosenTime[0] = hourOfDay;
            }
        };

        final TimePickerDialog startPickerDialog = new TimePickerDialog(
            Objects.requireNonNull(getContext()),
            startPickerDialogCallback,
            getHour(), 0, true);

        startSelector.setOnClickListener(new EditText.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPickerDialog.show();
            }
        });

        Button confirmButton = view.findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (chosenTime[0] < getHour() && Utils.simpleDate.format(chosenDate[0]).equals(getDate())) {
                new AlertDialog.Builder(getContext())
                    .setTitle("Warning")
                    .setMessage("Start time cannot be in the past.")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Ok
                        }
                    })
                    .show();
            }
            else {
                new AlertDialog.Builder(getContext())
                    .setTitle("Confirm booking?")
                    .setMessage("Are you sure you want to confirm a booking on " + Utils.simpleDate.format(chosenDate[0]) + " for " + chosenTime[0] + ":00 - " + getEndTime(chosenTime[0]) + ":00?")
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
                        StringRequest stringRequest = new StringRequest(
                            Request.Method.POST,
                            (Utils.baseApiUrl + "/book_room"),
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        new android.app.AlertDialog.Builder(getContext())
                                            .setTitle("Confirmed")
                                            .setMessage("The booking has been confirmed.")
                                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent intent = new Intent(getContext(), MainActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(intent);
                                                }
                                            })
                                            .create()
                                            .show();
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        new android.app.AlertDialog.Builder(getContext())
                                            .setTitle("Error")
                                            .setMessage("That slot is already taken. Please try another one.")
                                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Ok
                                                }
                                            })
                                            .create()
                                            .show();
                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("room", getName());
                                params.put("user", getResources().getString(R.string.user));
                                params.put("date", Utils.simpleDate.format(chosenDate[0]));
                                params.put("time", String.valueOf(chosenTime[0]));
                                return params;
                            }
                        };
                        requestQueue.add(stringRequest);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // No
                        }
                    })
                    .show();
            }
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }

    private String getName() {
        assert getArguments() != null;
        return getArguments().getString("name");
    }

    private int getHour() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    private String getDate() {
        return Utils.simpleDate.format(new Date());
    }

    private int getEndTime(int startTime) {
        if (startTime == 23) {
            return 0;
        }
        else {
            return startTime+1;
        }
    }
}