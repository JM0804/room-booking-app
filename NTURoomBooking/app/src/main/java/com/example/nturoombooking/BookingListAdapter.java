package com.example.nturoombooking;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class BookingListAdapter extends BaseAdapter {
    private Context context;
    private final List<Booking> bookings;

    public BookingListAdapter(Context context, List<Booking> bookings)  {
        this.context = context;
        this.bookings = bookings;
    }

    @Override
    public int getCount() {
        return bookings.size();
    }

    @Override
    public Booking getItem(int id) {
        return bookings.get(id);
    }

    public int getEndTime(int id) {
        int startTime = bookings.get(id).getTime();
        if (startTime == 23) {
            return 0;
        }
        else {
            return startTime+1;
        }
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public View getView(final int pos, @Nullable View convertView, @NonNull final ViewGroup parent) {
        ViewHolder viewHolder;

        Drawable drawableDateIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.CLOCK)
                .setColor(Color.LTGRAY)
                .build();

        @ColorInt int color = context.getResources().getColor(R.color.colorPrimary);

        Drawable drawableDeleteIcon = MaterialDrawableBuilder.with(context)
                .setIcon(MaterialDrawableBuilder.IconValue.DELETE)
                .setColor(color)
                .build();

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.single_bookings_list_item, parent, false);
            viewHolder.txtRoom = convertView.findViewById(R.id.room);
            viewHolder.txtDate = convertView.findViewById(R.id.date);

            viewHolder.imgDate = convertView.findViewById(R.id.date_icon);

            viewHolder.btnDelete = convertView.findViewById(R.id.delete);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtRoom.setText(getItem(pos).getRoom());
        viewHolder.txtDate.setText(String.format("%s, %s:00 - %s:00", getItem(pos).getDate(), getItem(pos).getTime(), getEndTime(pos)));

        viewHolder.imgDate.setImageDrawable(drawableDateIcon);

        viewHolder.btnDelete.setImageDrawable(drawableDeleteIcon);

        viewHolder.btnDelete.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Reference: https://stackoverflow.com/a/2478662
                DialogInterface.OnClickListener cancelDialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            // "Yes" button clicked
                            RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(context));
                            StringRequest stringRequest = new StringRequest(
                                    Request.Method.POST,
                                    (Utils.baseApiUrl + "/cancel_booking"),
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            new android.app.AlertDialog.Builder(context)
                                                .setTitle("Cancelled")
                                                .setMessage("The booking has been cancelled.")
                                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // Ok
                                                    }
                                                })
                                                .create()
                                                .show();
                                            bookings.remove(pos);
                                            notifyDataSetChanged();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            new android.app.AlertDialog.Builder(context)
                                                .setTitle("Error")
                                                .setMessage(error.toString())
                                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // Ok
                                                    }
                                                })
                                                .create()
                                                .show();
                                        }
                                    }
                            ) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();
                                    params.put("id", String.valueOf(getItem(pos).getId()));
                                    return params;
                                }
                            };
                            requestQueue.add(stringRequest);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            // "No" button clicked
                            break;
                    }
                    }
                };

                final AlertDialog.Builder cancelDialog = new AlertDialog.Builder(context/*, R.style.AlertDialogCustom*/);
                String message = String.format("%s: %s, %s:00 - %s:00", getItem(pos).getRoom(), getItem(pos).getDate(), getItem(pos).getTime(), getEndTime(pos));
                cancelDialog.setMessage("Are you sure you want to cancel this booking?\n\n" + message)
                    .setTitle("Cancel booking?")
                    .setPositiveButton("Yes", cancelDialogClickListener)
                    .setNegativeButton("No", cancelDialogClickListener)
                    .show();
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView txtRoom;
        TextView txtDate;
        ImageView imgDate;
        ImageButton btnDelete;
    }
}