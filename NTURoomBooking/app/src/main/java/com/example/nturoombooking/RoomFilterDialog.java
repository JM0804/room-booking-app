package com.example.nturoombooking;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RoomFilterDialog extends DialogFragment {
    public static final String TAG = "RoomFilterDialog";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.room_list_filter_dialog, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_mdi_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        toolbar.setTitle("Filter Rooms");

        EditText minimumCapacity = view.findViewById(R.id.minimum_capacity);
        minimumCapacity.setTransformationMethod(null);

        EditText minimumComputers = view.findViewById(R.id.minimum_computers);
        minimumComputers.setTransformationMethod(null);

        Spinner typeDropdown = view.findViewById(R.id.type);
        String[] types = {"Laboratory", "Lecture Hall", "Seminar Room"};
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_dropdown_item, types);
        typeDropdown.setAdapter(typeAdapter);

        Spinner buildingDropdown = view.findViewById(R.id.building);
        List<Building> buildingsList = new Gson().fromJson(
                Utils.getJsonFromAssets(Objects.requireNonNull(getContext()), "buildings.json"),
                new TypeToken<List<Building>>() {}.getType()
        );

        ArrayList buildingsArrayList = new ArrayList<String>();

        assert buildingsList != null;
        for (Building building : buildingsList) {
            buildingsArrayList.add(building.getNameLong());
        }

        String[] buildings = new String[buildingsArrayList.size()];
        buildingsArrayList.toArray(buildings);

        ArrayAdapter<String> buildingAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, buildings);
        buildingDropdown.setAdapter(buildingAdapter);

        Button resetButton = view.findViewById(R.id.reset_button);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Button cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button confirmButton = view.findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);
        }
    }
}
