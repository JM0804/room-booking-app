# Room Booking App

Room Booking App for Nottingham Trent University's Clifton Campus, created for my final year Mobile Platform Applications (ITEC31041) module.

Works in conjunction with the [Room Booking Server](https://gitlab.com/JM0804/room-booking-server/)


## Set-up

- Create the following file: `/NTURoomBooking/app/src/main/res/values/developer_config.xml`

- Insert the following into the file (adding in your Mapbox access token):

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="mapbox_access_token">YOUR_TOKEN_HERE</string>
</resources>
```
